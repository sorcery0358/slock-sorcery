# slock-sorcery
a custom build of suckless's dwm (simple display locker for X) <br>
it is suitable with the colors of matcha gtk theme; <br>
https://github.com/vinceliuice/Matcha-gtk-theme
###
## recommended to use it with these custom builds of suckless software: 
dmenu-sorcery; https://gitlab.com/sorcery0358/dmenu-sorcery <br>
st-sorcery; https://gitlab.com/sorcery0358/st-sorcery <br>
dwm-sorcery; https://gitlab.com/sorcery0358/dwm-sorcery
## patches
this custom build of st includes these patches:
###
dpms; https://tools.suckless.org/slock/patches/dpms <br>
foreground and background; https://tools.suckless.org/slock/patches/foreground-and-background <br>
pam auth; https://tools.suckless.org/slock/patches/pam_auth <br>
quickcancel; https://tools.suckless.org/slock/patches/quickcancel <br>
user; https://tools.suckless.org/slock/patches/user
## installation
	$ cd slock-sorcery/slock-1.4
	$ sudo make clean install
## customization
to customize edit `slock-sorcery/slock-1.4/config.def.h` and run these commands;
###
	$ sudo cp config.def.h config.h
	$ sudo make clean install
##
`slock-sorcery/slock-1.4.tar.gz` is the vanilla version of slock
